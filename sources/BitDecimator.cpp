#include "BitDecimator.h"
#include <algorithm>
#include <cstring>
#include <cmath>

void BitDecimator::init(double sampleRate)
{
    fSampleTime = 1.0 / sampleRate;

    fHpf.init(sampleRate);
    fHpf.setCutoff(20.0);

    static constexpr double coefs2x[12] = { 0.036681502163648017, 0.13654762463195794, 0.27463175937945444, 0.42313861743656711, 0.56109869787919531, 0.67754004997416184, 0.76974183386322703, 0.83988962484963892, 0.89226081800387902, 0.9315419599631839, 0.96209454837808417, 0.98781637073289585 };
    fDownsampler2x.set_coefs(coefs2x);
}

void BitDecimator::clear()
{
    fPhase = 0.0;
    fLastValue = 0.0;
    fDownsampler2x.clear_buffers();
    fHpf.clear();
}

void BitDecimator::setDepth(float depth)
{
    fDepth = std::max(0.0f, std::min(100.0f, depth));
}

void BitDecimator::process(const float *in, float *out, uint32_t nframes)
{
    float depth = fDepth;

    if (depth == 0) {
        std::memcpy(out, in, nframes * sizeof(float));
        clear();
        return;
    }

    float dt;
    {
        // exponential curve fit
        float a = 5.729950e+04, b = -6.776081e-02, c = 180;
        float freq = a * std::exp(fDepth * b) + c;
        dt = freq * fSampleTime;
    }

    OnePoleHPF &hpf = fHpf;
    float phase = fPhase;
    float lastValue = fLastValue;
    hiir::Downsampler2xFpu<12> &downsampler2x = fDownsampler2x;

    for (uint32_t i = 0; i < nframes; ++i) {
        float x = in[i];

        phase += dt;
        float y = (phase > 1.0f) ? x : lastValue;
        phase -= (int)phase;

        float y2x[2];
        y2x[0] = (y != lastValue) ? (0.5f * (y + lastValue)) : y;
        y2x[1] = y;

        lastValue = y;

        y = downsampler2x.process_sample(y2x);
        y = hpf.process(y);
        out[i] = y;
    }

    fPhase = phase;
    fLastValue = lastValue;
}
