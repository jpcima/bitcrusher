#pragma once
#include "OnePoleFilter.h"
#include "hiir/Downsampler2xFpu.h"
#include <cstdint>

class BitReducer {
public:
    void init(double sampleRate);
    void clear();
    void setDepth(float depth);
    void process(const float *in, float *out, uint32_t nframes);

private:
    float fDepth = 0.0;
    float fLastValue = 0.0;
    hiir::Downsampler2xFpu<12> fDownsampler2x;
    OnePoleHPF fHpf;
};
