/*
 * Bit reducer audio effect based on DISTRHO Plugin Framework (DPF)
 *
 * SPDX-License-Identifier: BSL-1.0
 *
 * Copyright (C) 2019 Jean Pierre Cimalando <jp-dev@inbox.ru>
 */

#ifndef PLUGIN_BITREDUCER_H
#define PLUGIN_BITREDUCER_H

#include "DistrhoPlugin.hpp"
#include "BitReducer.h"
#include "BitDecimator.h"
#include <memory>

// -----------------------------------------------------------------------

class PluginBitreducer : public Plugin {
public:
    enum Parameters {
        paramReducerDepth,
        paramDecimatorDepth,
        paramWetGain,
        paramDryGain,
        paramCount
    };

    PluginBitreducer();

protected:
    // -------------------------------------------------------------------
    // Information

    const char* getLabel() const noexcept override {
        return "Bitreducer";
    }

    const char* getDescription() const override {
        return "Bit reducer";
    }

    const char* getMaker() const noexcept override {
        return "JPC";
    }

    const char* getHomePage() const override {
        return "https://example.com/plugins/bitcrushing";
    }

    const char* getLicense() const noexcept override {
        return "https://spdx.org/licenses/BSL-1.0";
    }

    uint32_t getVersion() const noexcept override {
        return d_version(0, 1, 0);
    }

    // Go to:
    //
    // http://service.steinberg.de/databases/plugin.nsf/plugIn
    //
    // Get a proper plugin UID and fill it in here!
    int64_t getUniqueId() const noexcept override {
        return d_cconst('J', 'b', 'i', 't');
    }

    // -------------------------------------------------------------------
    // Init

    void initParameter(uint32_t index, Parameter& parameter) override;

    // -------------------------------------------------------------------
    // Internal data

    float getParameterValue(uint32_t index) const override;
    void setParameterValue(uint32_t index, float value) override;

    // -------------------------------------------------------------------
    // Optional

    // Optional callback to inform the plugin about a sample rate change.
    void sampleRateChanged(double newSampleRate) override;

    // -------------------------------------------------------------------
    // Process

    void activate() override;

    void run(const float** inputs, float** outputs, uint32_t frames) override;

private:
    enum { BufferLimit = 512 };
    void runWithinBufferLimit(const float** inputs, float** outputs, uint32_t frames);

    // -------------------------------------------------------------------

private:
    float    fParams[paramCount];
    double   fSampleRate;

    std::unique_ptr<float[]> fBufferTemp;

    enum { kChannels = DISTRHO_PLUGIN_NUM_INPUTS };
    static_assert(kChannels == DISTRHO_PLUGIN_NUM_OUTPUTS, "Channel counts do not match");

    BitReducer fBitReducer[kChannels];
    BitDecimator fBitDecimator[kChannels];

    DISTRHO_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(PluginBitreducer)
};

// -----------------------------------------------------------------------

#endif  // #ifndef PLUGIN_BITREDUCER_H
