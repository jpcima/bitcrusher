/*
 * Bit reducer audio effect based on DISTRHO Plugin Framework (DPF)
 *
 * SPDX-License-Identifier: BSL-1.0
 *
 * Copyright (C) 2019 Jean Pierre Cimalando <jp-dev@inbox.ru>
 */

#include <cmath>

#include "PluginBitreducer.hpp"

// -----------------------------------------------------------------------

PluginBitreducer::PluginBitreducer()
    : Plugin(paramCount, 0, 0),
      fBufferTemp{new float[BufferLimit]}
{
    sampleRateChanged(getSampleRate());

    for (uint32_t i = 0; i < paramCount; ++i) {
        Parameter p;
        initParameter(i, p);
        setParameterValue(i, p.ranges.def);
    }
}

// -----------------------------------------------------------------------
// Init

void PluginBitreducer::initParameter(uint32_t index, Parameter& parameter) {
    DISTRHO_SAFE_ASSERT_RETURN(index < paramCount, );

    parameter.hints = kParameterIsAutomable;

    switch (index) {
    case paramReducerDepth:
        parameter.name = "Bit reducer depth";
        parameter.symbol = "reducer_depth";
        parameter.ranges = ParameterRanges(50.0, 0.0, 100.0);
        parameter.unit = "%";
        break;
    case paramDecimatorDepth:
        parameter.name = "Bit decimator depth";
        parameter.symbol = "decimator_depth";
        parameter.ranges = ParameterRanges(50.0, 0.0, 100.0);
        parameter.unit = "%";
        break;
    case paramWetGain:
        parameter.symbol = "wet_gain";
        parameter.name = "Wet gain";
        parameter.hints = kParameterIsAutomable;
        parameter.unit = "dB";
        parameter.ranges = ParameterRanges(0.0, -60.0, +20.0);
        break;
    case paramDryGain:
        parameter.symbol = "dry_gain";
        parameter.name = "Dry gain";
        parameter.hints = kParameterIsAutomable;
        parameter.unit = "dB";
        parameter.ranges = ParameterRanges(-60.0, -60.0, +20.0);
        break;
    }
}

// -----------------------------------------------------------------------
// Internal data

/**
  Optional callback to inform the plugin about a sample rate change.
*/
void PluginBitreducer::sampleRateChanged(double newSampleRate) {
    fSampleRate = newSampleRate;

    for (uint32_t c = 0; c < kChannels; ++c) {
        fBitReducer[c].init(newSampleRate);
        fBitDecimator[c].init(newSampleRate);
    }
}

/**
  Get the current value of a parameter.
*/
float PluginBitreducer::getParameterValue(uint32_t index) const {
    DISTRHO_SAFE_ASSERT_RETURN(index < paramCount, 0.0);

    return fParams[index];
}

/**
  Change a parameter value.
*/
void PluginBitreducer::setParameterValue(uint32_t index, float value) {
    DISTRHO_SAFE_ASSERT_RETURN(index < paramCount, );

    fParams[index] = value;
}

// -----------------------------------------------------------------------
// Process

void PluginBitreducer::activate() {
    // plugin is activated
}

void PluginBitreducer::run(const float **inputs, float **outputs, uint32_t frames)
{
    for (uint32_t index = 0; index < frames; ) {
        uint32_t curr_frames = std::min(frames - index, (uint32_t)BufferLimit);
        const float *curr_inputs[kChannels];
        float *curr_outputs[kChannels];

        for (unsigned c = 0; c < kChannels; ++c) {
            curr_inputs[c] = inputs[c] + index;
            curr_outputs[c] = outputs[c] + index;
        }

        runWithinBufferLimit(curr_inputs, curr_outputs, curr_frames);
        index += curr_frames;
    }
}

void PluginBitreducer::runWithinBufferLimit(const float** inputs, float** outputs, uint32_t frames)
{
    float brDepth = fParams[paramReducerDepth];
    float bdDepth = fParams[paramDecimatorDepth];
    float wetGain = std::pow(10.0f, 0.05f * fParams[paramWetGain]);
    float dryGain = std::pow(10.0f, 0.05f * fParams[paramDryGain]);

    float *temp = fBufferTemp.get();

    for (uint32_t c = 0; c < kChannels; ++c) {
        const float *const inp = inputs[c];
        float *const out = outputs[c];

        BitReducer &br = fBitReducer[c];
        br.setDepth(brDepth);
        br.process(inp, temp, frames);

        BitDecimator &bd = fBitDecimator[c];
        bd.setDepth(bdDepth);
        bd.process(temp, temp, frames);

        for (uint32_t i = 0; i < frames; ++i)
            out[i] = temp[i] * wetGain + inp[i] * dryGain;
    }
}

// -----------------------------------------------------------------------

Plugin* DISTRHO::createPlugin() {
    return new PluginBitreducer();
}

// -----------------------------------------------------------------------
